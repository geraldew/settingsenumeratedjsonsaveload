# Settings Enumerated JSON Save Load

This project is a demonstration example of a technique for handling settings, such that:
- some setting keys and values are Python-specific - e.g. enumerations
- settings can be exported to a JSON file
- settings can be imported from a JSON file
- although JSON only handles a restricted subset of types, the method provides a way for some Python types to be brought in and restored to their Python form.

If it proves successful then it may become a stock module to be imported and used in other Python programs.

## Current status

It is now being adapted from proof-of-concept to something practical for general sharing - and perhaps to then publish an article for discussion.

At this point the concept seems to be proven and the code is sound and sufficiently functional.

Some obvious features still missing are:
- a better way to have it cover known or specified enumerations - they are currently hard-coded in this module
- extended to handle more Python data types: e.g. boolean, set and float

## Why write this?

The idea of this came up during two quite unrelated things that I was writing, one in my personal life, and one during the course of my employment.

Parts of the idea were formed in either/both of those locations. While the "at work" me had a feeling that I could find a solution to the enumeration-JSON part, as I couldn't guess how long or how much work it might take, I decided to let the "at home" me put in the time/effort to achieve the proof of concept and then an implementation.

Which is a long way to explain why I feel free to put this code out in public under an Open Source license.

Do note that I am not claiming this to be the best, most Pythonic or in any way ideal solution. It is merely something I came up with and am openly sharing as Open Source.

I also had some reasons for not seeking to pull in code, or modules from elsewhere to tackle the problem (but alas, not such that I can say much about those reasons here).

As events turned out, the "at work" project was over before I was happy with the state of this one.

## Brief

While the core feature here is about storing and retrieving settings as JSON, the concept includes the idea of also having "hard-coded" settings inside an application.

The general problem situation is meant to tackle has these features:
- in Python, having a *good-enough* way to set up a number of **preset** values (or structures of values) by which to control parts of the program. You could perhaps think of that as an internal "config" for the program.
- in particular, how to do that without any dependencies on modules other than those almost certain to be present in almost any Python installation
- how to integrate that internal config idea to also be quite amenable to being read in from a separate text-based configuration file (with an assumption of that being done in JSON format)
- the solution needed to be able to handle "values" that are Python enumerations - indeed it was this part that I didn't find available in anything I could find online.

This program is both an experiment and a demonstration. It is not claimed to be production ready code.

## License

This code is licensed with GPL3, which is what I usually apply to my Open Source works.

Of course in this case, it is a fairly small amount of code and is made available to demonstrate techniques and ideas. Thus in practise, to abuse copyright for this code would really only be about taking the whole of it and claiming as yours and/or without respecting the license. 

So in short: please DO use these ideas and techniques to write *your own code*, even if - perhaps inevitably - some parts will look quite similar to this body of code.

Of course, if you want to adapt and/or extend this whole example then just respect the license and all will be fine for everyone.

## Types Handled

In many senses, the issue here is mapping between the smaller number of data types that JSON handles with the more complex and subtle types that Python allows.
- with a particular focus on the fact that you might want to use those subtle Python types as "settings" - both inside the program and as something stored as JSON  

### As Values

To quote from JSON documentation: In JSON, values must be one of the following data types:
- a string
- a number - Numbers in JSON must be an integer or a floating point
- an object (i.e. another JSON object)
- an array
- a boolean 
- null

These have fairly direct counterparts in Python:
- string ->- str
- number ->- int
- number ->- float
- object ->- dict
- array ->- list
- boolean (true|false) ->- Boolean (True|False)
- null ->- None

But we would also like to handle:
- Enumeration
- Set
- Tuple
- Named Tuple

Although handling Tuple & Named Tuple can probably be deferred indefinitely - not least becaause they have no obvious encoding mechanism for serialisation into JSON. 

### As Keys

Here we would like to handle one additional type as a "key" that  

- Enumeration

Notes:
- in Python dictionaries, only an immutable value can be used as a key
- in Python sets, only an immutable value can be a member
- ruled out therefore are lists and dictionaries and sets, as these are all mutable


## A Deeper Description

For simplicity, the whole thing is presented as a single Python module - but the concept was conceived for program structures where some parts are in other modules.

There are several things going on this example, that are then combined to make a reusable technique.

The various elements are:
- there are some definitions of enumeration-types - to be a good example, there are two of these used here
- some enumerations appear as "keys" in dictionaries, some as "values" in dictionaries - this is to serve the fact that Python allows an enumeration to be used as a key (whereas other languages require keys to be _simple_ data types)
- the example has just one configuration settings logical structure that is being managed
- there is one hard-coded "preset" structure that uses the enumerations and provides a hard-coded set of configuration settings
- there is also one *text only* counterpart structure - this is to allow the example to demonstrate converting a text-only structure into an enumeration-based structure (for this being an example, this acts as a proxy for a loaded JSON, allowing demonstration even when there is no JSON file to load)
- there is a _*pro forma*_ structure that acts as a guide for the conversion from text to enumerations throughout the structure
- there are some functions for pulling out values from the "preset" structure - this provides some abstraction, so that an application can call these without needing to know how the "preset" works internally 
- there is some code to handle the generic reading and writing of JSON files - really just some failsafe wrapping of the stock Python JSON file functions
- there is the crtical code section, that takes a text-only structure, a pro forma structure, and returns a structure with the values from the text-only but with controlled parts converted into enumerations. In functional terms this is the same as the preset but has been populated from the JSON (or the text-only substitute)
- finally various amounts of boring code just to orchestrate all that.

At the time of writing, the code is provided as two parallel texts:
- one is padded out with a lot of `if debug` sections. These were used during development and have merely been left in place.
- there is also a copy that has had all the `if debug` sections removed.

Similarly, the code has not been optimised AT ALL - so caveat emptor. 

### Enumerations

#### Tricky to talk about

In these notes it will be presumed that you may go elsewhere to understand what enumerations are in stock Python.

Somewhat annoying is that linguistically, it can be unclear exaclty what it means to say that something is "an enumeration" in Python. For instance, does that refer to:
- a custom class definition that inherits from `Enum` ? - is that "an enumeration" ?
- an instance variable of that newly defined class ? - is that "an enumeration" ?

For what it's worth, Python has a simlar problem with Named Tuples.

I prefer to think of the defined class as creating a data type - so I will (try to) call that, *"an enumeration-type"*. That then lets me use the term "an enumeration" for the instances of an enumeration-type.

To clarify that, let's look at a typical use of enumeration. In the following code, we define an enumeration-type named `InGroupMethod`

``` Python
@unique
class InGroupMethod(Enum):
	ByAge = auto() 
	ByAlpha = auto() 
	ByNumeric = auto() 
	ByPathLength = auto() 
	ByPathDepth = auto() 
``` 

At that point in the code, all that's happened is that a new `class` has been defined and named. In this case it's a simple one based on the `Enum` class - that had to be imported, perhaps as a line with: `from enum import Enum, unique, auto` 

As it happens, this definition is also making use of `unique` as a decorator and `auto` to provide unseen values.

We don't get to have an actual Python object of this new data type until we use it for assigning to a variable, such as:


``` Python
DoByWhich_InGroupMethod = InGroupMethod.ByAge
``` 

This means that the variable `DoByWhich_InGroupMethod` is holding *"an enumeration"* - which despite how simply we are treating it, is actually a compound Python object.

If we want to test whether a given variable, `e` is holding a specific *"enumeration identity"* we can use the same syntax for it.

``` Python
if e == InGroupMethod.ByAge :
	ProcessThingByAge( thing)
``` 

Note, because the enumeration is itself a complex object, it happens to provide a property of "value". If instead of comparing the variable to the *"enumeration identity"* we want to compare to, or to see the value of the enumeration identity

``` Python
print( InGroupMethod.ByAge.value)
``` 

or 

``` Python
n = 1
if n == InGroupMethod.ByAge.value :
	pass
``` 

So, what we have from that are:
- enumeration-type - is the new class definition
- enumeration identity - is one of the possible members
- enumeration value - is the scalar value for one of the enumeration identities


So, to the enumeration-types then. In the example there are two of these, 

- `class enum_LeMode( str, Enum):`
- `class enum_Version( str, Enum):`


Structurally therefore these:
- have *named* elements, which can be used explicitly in the Python program script - e.g. `enum_LeMode.Dev`
- have an internal *value* for each element, in particular ...
- using explicit string *values* (which is to say, rather than either numbers or `auto()` or other things allowed in Python enumerations)
- which is why they have a dual inheritance as classes - both `str` and `Enum`
- the `str` inheritance enables some shortcuts in how they are handled (and which may also mean that some adaptations might be required for _numeric_ or _auto_ enumeration-types but I'll cross that bridge later)

Note that the class definitions are followed by some addtional supporting functions - these are carry overs from my usual way of using enumeration-types - only some of those are important in this example. I will probably trim those out at some point.

If not already apparent, I've put in *two* enumeration-types so that in the example one gets used for "keys" and one gets used for "values" in the structures.

### Multiple Presets

In this example, there is just one bunch of "settings". Nothing really to say about this, but it happens that the "at work" target for this technique has three of these (one each for system settings, setup settings and current-run settings).

### The Trick in the Tail

To briefly ignore all the detail - what is the core magic behind all this? Well, not magic at all of course, but the crucial parts are:

- ability to use a plain value to set an enumeration variable, as per:

``` Python
r_outcoming = enum_LeMode( p_incoming)
``` 

- the fiddly matter of detecting when the ProForma says there should be an enumeration, as done in the function `MatchKey_InKey_DctProforma` with particular lines like:

``` Python
if isinstance( i_key_PPF_consider, Enum):
``` 

and

``` Python
i_lst_key_enumerations = list( type( i_key_PPF_consider ) )
``` 

It is that which allows the PythonProForma to be a much simpler structure than the ones it acts as guides for - i.e. the Preset or the Text-Only.


### Literal Preset

A core idea of how this is all to work, is that inside some module, there is a variable defined as being in global-to-the-module-scope. In effect, this is to be treated as being a "constant" - in that nothing should ever write to it. This is "the literal preset" and is a Python dictionary, which may contain values, lists and sub-dictionaries.

Note:
- to say that Python has a problem with 'constants" is an understatement. In practice, the concept just doesn't exist.
- the usual convention in Python is to put things meant to be "constant" into variables with names in all uppercase. I don't like that convention so I just don't use it. I'm not going to argue about it, feel free to clone the code and rename the things you want to pretend to be constants however you wish.
- do note that the preset is intended to allow enumerations to be used as either/both values and keys.  

As mentioned already, this example includes something that wouldn't be there in a real usage of this technique, which is a text-only counterpart of the preset. This effectiely emulates what you'd have after ingesting a JSON file. Its presence is to allow testing without having to first make a JSON file from which to import. However, as the code to saving and reading a JSON file is also included, either action sequence can be trialled.

Hence, what you'll see in the example are multiple instances of presets:
- `Literal_Specific_Enum` - this is the structured and typed
- `Literal_Specific_AllStrings` - this is the text only counterpart. BTW this is shown first in the code as its easier-to-read and thus gives you a clearer idea of what's going on.
- `The_Specific` - this is the module-level current value that the module will work with.

There is a line of code - that is outside of any `def` sections, meaning that it gets executed at module initialisation. i.e. 
- `The_Specific = Literal_Specific_Enum`

### JSON handling

For reasons relating to the structure code from which this example was adapted, the handling of JSON files is done as two layers of functions. This probably looks like overkill, but bear in mind that one of the real world uses for this was a program that managed three sets of settings. Hence the outer layer was an orchestration of multiple JSON files. In this example the same layer is present but only has one call to make, so it looks superfluous.

### The Proforma Conversion

This is a set of three functions that perform the business of using one Preset as a guide to convert a text-only structure into one that has the apprpriate enumerations.

- `def ConvertIncomingToProformaEnumeration( p_incoming, p_proforma ):`
- `def MatchKey_InKey_DctProforma( p_in_key, p_dct_proforma):`
- `def Dictionary_ProForma_Load( p_dct_proforma, p_dct_incoming, p_depth ):`

The first two of those could have been written as being interal to the last one - as they are not intended to be called indepedently.

As you might gather from the naming, the idea is to pass in two dictionaries:
- one that is structured with the desired variable data types - this is the "Pro Forma"
- one that has the same structures but all the values are just text - such as would be "Incoming" from reading a JSON file

The idea is to then return a new Preset structure that has the same data types as the pro forma but values informed by the "incoming".

### Proforma Conversion Strategies

There isn't a single way to approach this process. At the least there are:
- ensure all aspects of the Pro Forma are retained - so if there are elements missing in the Incoming then they are supplied from the Pro Forma
- ensure that only aspects provided in the Incoming are returned as a structure - THIS IS THE CURRENT IMPLEMENTATION

Note:
- Not even considered in the coding is the idea of allowing through any elements in the Incoming that have no counterparts in the Pro Forma. 

Ultimately, the goal here is to be a simple technique. To enable more sophisticated options would seem to be the territory of handling as XML and perhaps using an XML Schema and possibly validation against such.

## Deficiencies

I've elected to share this code from as soon as I had the proof-of-concept worked out. However, that does mean some aspects of it are clearly not as good as we'd probably like them to be.

The main thing I'm not fully happy with, is the function `ConvertIncomingToProformaEnumeration` which needs to explicitly know and handle each enumerated type in literal code.

## Run Examples

This is what happens when running the function `module_test_check`

First you get a `print()` of the preset structure `Literal_ProForma_Enum` - this has a minimal set of elements but does have Enumerations to act as a guide to data types - and which shows as:

``` Python
{<enum_LeMode.Dev: 'Development'>: {'Android': {'VersionName': <enum_Version.Ice: 'IceCreamSandwich'>}, 'Owner': 'Eric'}}
``` 

Note there, that some keys are Enumerations while some are strings. Also that some values are Enumerations while some are strings. 

Then you get a `print()` of the preset structure `Literal_Specific_Enum` - this is a full amount of settings and is what the program is meant to use in case there are no JSON configuration files to be loaded - and which shows as:

``` Python
{<enum_LeMode.Prod: 'Production'>: {'Android': {'VersionName': <enum_Version.Gin: 'Gingerbread'>}}, <enum_LeMode.Prep: 'PreProd'>: {'Android': {'VersionName': <enum_Version.Hon: 'Honeycomb'>}, 'Owner': '?'}, <enum_LeMode.Dev: 'Development'>: {'Android': {'VersionName': <enum_Version.Ice: 'IceCreamSandwich'>}, 'Owner': '?'}}
``` 

Note that the values for the key of "Owner" are quoted question marks. In this example, this a way to imply that while this built-in structure does have "Owner" keys, the values are expected to replaced by something read in from file. The question marks themselves don't mean anything - no part of this program uses them - so these are just a kind of "note to self".

- However this does also represent a point a design choice. An alternative would to not even have that key present, but then that would require the program to accept in keys 


Then you get a `print()` of the preset structure `Literal_Specific_AllStrings` - this acts as an example of what you might get if you loaded from a JSON file, in that all the keys and values are strings - and which shows as:

``` Python
{'Production': {'Android': {'VersionName': 'Honeycomb'}}, 'PreProd': {'Android': {'VersionName': 'IceCreamSandwich'}, 'Owner': 'Wendy'}, 'Development': {'Android': {'VersionName': 'Gingerbread'}, 'Owner': 'Sally'}}
``` 

Finally, you get a `print()` of the structure that gets created by calling:

``` Python
New_Specific = Make_Dictionary_From_Incoming_AsPer_ProForma( Literal_Specific_AllStrings, Literal_ProForma_Enum, 1 )`
``` 

and so you get a `print()` of that structure: `New_Specific`

``` Python
{<enum_LeMode.Prod: 'Production'>: {'Android': {'VersionName': <enum_Version.Hon: 'Honeycomb'>}}, <enum_LeMode.Prep: 'PreProd'>: {'Android': {'VersionName': <enum_Version.Ice: 'IceCreamSandwich'>}, 'Owner': 'Wendy'}, <enum_LeMode.Dev: 'Development'>: {'Android': {'VersionName': <enum_Version.Gin: 'Gingerbread'>}, 'Owner': 'Sally'}}
``` 

which should be a reworking of the `Literal_Specific_AllStrings` structure and values so that it uses Enumerations in the same places as indicated by the "ProForma" object.
