#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Settings Enumerated JSON Save Load = an example of technique 
#
# --------------------------------------------------
# Copyright (C) 2022  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

from enum import Enum, unique, auto
import json as im_json

# --------------------------------------------------
# Enumerations
# --------------------------------------------------

# --------------------------------------------------
# Mode

@unique
class enum_LeMode( str, Enum):
	Prod = "Production"
	Prep = "PreProd"
	Dev = "Development"

def eLeMode_Default() :
	return enum_LeMode.Dev

# the rest of these are just stock items, probably not needed for this project

def eLeMode_AsList() :
	return list( map( lambda c: c.value, enum_LeMode ) )

def eLeMode_AsTuple() :
	return tuple( LeMode_GetList() )

def eLeMode_EnumOfString( s ) :
	m = LeMode_Default()
	for member in enum_LeMode :
		if member.value == s :
			m = member
			break
	return m

def eLeMode_EnumOfName( s ) :
	# Usage: eok, enm = LeMode_EnumOfName( s ) 
	eok = False
	for member in enum_LeMode :
		if member.name == s :
			enm = member
			eok = True
			break
	return eok, enm

def eLeMode_EnumValueString( e ) :
	try:
		s = e.value
	except:
		s = ""
	return s

_dct_eLeMode_ShowStrings = { 
	enum_LeMode.Prod : "Production", 
	enum_LeMode.Prep : "Pre-Production", 
	enum_LeMode.Dev : "Development" }

def eLeMode_ShowStr( p_eLeMode ):
	if p_eLeMode in _dct_eLeMode_ShowStrings :
		return _dct_eLeMode_ShowStrings[ p_eLeMode ]
	else :
		return "Unknown!"

# --------------------------------------------------
# Android Version

@unique
class enum_Version( str, Enum):
	Gin = "Gingerbread"
	Hon = "Honeycomb"
	Ice = "IceCreamSandwich"

def eVersion_Default() :
	return enum_Version.Ice

# the rest of these are just stock items, probably not needed for this project

def eVersion_AsList() :
	return list(map(lambda c: c.value, enum_Version))

def eVersion_AsTuple() :
	return tuple( Version_GetList() )

def eVersion_EnumOfString( s ) :
	m = Version_Default()
	for member in enum_Version :
		if member.value == s :
			m = member
			break
	return m

def eVersion_EnumOfName( s ) :
	# Usage: eok, enm = Version_EnumOfName( s ) 
	eok = False
	for member in enum_Version :
		if member.name == s :
			enm = member
			eok = True
			break
	return eok, enm

def eVersion_EnumValueString( e ) :
	try:
		s = e.value
	except:
		s = ""
	return s

_dct_eVersion_ShowStrings = { 
	enum_Version.Gin : "Gingerbread", 
	enum_Version.Hon : "Honeycomb", 
	enum_Version.Ice : "Ice Cream Sandwich" }

def eVersion_ShowStr( p_eVersion ):
	if p_eVersion in _dct_eVersion_ShowStrings :
		return _dct_eVersion_ShowStrings[ p_eVersion ]
	else :
		return "Unknown!"

# --------------------------------------------------
# Generic JSON Save and Load
# --------------------------------------------------

def SaveIntoJsonFile( dct_obj, str_fileref):
	defname = "SaveIntoJsonFile" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN " + str_fileref )
	didok = False
	if len( dct_obj ) > 0 :
		if True : # place to put file exists check
			#try :
			if True :
				with open( str_fileref, 'w') as outfile:
					#try :
					if True :
						im_json.dump( dct_obj, outfile)
						didok = True
					#except: 
					else : 
						pass
						print( defname + "Failed json.dump for file: " + str_fileref )
			#except: 
			else : 
				pass
				print( defname + "Failed file open for write" )
	else :
		print( defname + "Won't save an empty object" )
	if debug :
		print( defname + "ENDED" )
	return didok

def LoadFromJsonFile( str_fileref ) :
	defname = "LoadFromJsonFile" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN " + str_fileref )
	gotok = False
	incoming = None
	if len( str_fileref ) > 0 :
		if True : # place to put file exists check
			#try :
			if True : 
				with open( str_fileref, "r") as read_file:
					#try :
					if True : 
						incoming = im_json.load( read_file )
						gotok = True
					#except: 
					else : 
						pass
						print( defname + "Failed json.load for file: " + str_fileref )
			#except: 
			else : 
				pass
				print( defname + "Failed file open for read" )
	else :
		print( defname + "No filename supplied" )
	if debug :
		print( defname + "ENDED" )
	return gotok, incoming

# --------------------------------------------------
# Specific Values
# --------------------------------------------------

def DctKey_ShoeSize() :
	return "ShoeSize"

def DctKey_Owner() :
	return "Owner"

def DctKey_IsOk() :
	return "IsOk"

def DctKey_Android() :
	return "Android"

def DctKey_Version() :
	return "VersionName"

def DctKey_ListOfStr() :
	return "ListOfStr"

def DctKey_ListOfInt() :
	return "ListOfInt"

# --------------------------------------------------
# Specific Literals
# --------------------------------------------------

# One to be a pro forma - this will need to have sufficient instances of the possible enumerations
Literal_ProForma_Enum = {
	enum_LeMode.Dev : {
		DctKey_ShoeSize() : 7 ,
		DctKey_Android() : {
			DctKey_Version() : enum_Version.Ice } ,
		DctKey_Owner() : "Eric" ,
		DctKey_ListOfStr() : [ "Aleph" ] ,
		DctKey_ListOfInt() : [ 0 ] , 
		DctKey_IsOk() : False } }

# One to be an example of what you might get when reading in from JSON
Literal_Specific_AllStrings = { 
	"Production" : { 
		"Android" : { 
			"VersionName" : "Honeycomb" } ,
		"ShoeSize" : "7" } , 
	"PreProd" : { 
		"Android" : { 
			"VersionName" : "IceCreamSandwich" },
		"Owner" : "Wendy",
		"ListOfInt" : [ "1", "2", "3", "5" ] } , 
	"Development" : { 
		"Android" : { 
			"VersionName" : "Gingerbread" },
		"Owner" : "Sally",
		"ListOfStr" : [ "Alpha", "Beta", "Gamma" ] ,
		"IsOk" : "true" } }

# One to be an internally known (aka a default) setting to use in the absenc of any JSON source
Literal_Specific_Enum = { 
	enum_LeMode.Prod : { 
		DctKey_Android() : { 
			DctKey_Version() : enum_Version.Gin } ,
		DctKey_ShoeSize() : 7 } , 
	enum_LeMode.Prep : { 
		DctKey_Android() : { 
			DctKey_Version() : enum_Version.Hon },
			DctKey_Owner() : "?",
			DctKey_ListOfInt() : [ 1, 2, 3, 5 ] } , 
	enum_LeMode.Dev : { 
		DctKey_Android() : { 
			DctKey_Version() : enum_Version.Ice }, 
			DctKey_Owner() : "?" ,
		DctKey_ListOfStr() : [ "Alpha", "Beta", "Gamma" ] ,
		DctKey_IsOk() : True } }
# indeed, that Specific might be used to generate an initial JSON file that can then be edited outside the program

# --------------------------------------------------
# Set to Literal as default value
# --------------------------------------------------

The_Specific = Literal_Specific_Enum

# --------------------------------------------------
# Calls for Values
# These are the preferred way to fetch values while in other modules
# So that only this module needs to know how the structures are really stored
# --------------------------------------------------

# --------------------------------------------------
# SystemLevel calls

def Get_Specific_Android_Version( p_Mode ):
	i_str_Mode = ( p_Mode )
	i_str_OS = DctKey_Android()
	return The_Specific[ i_str_Mode ][ i_str_OS ]

# --------------------------------------------------
# Load from and Save to File
# --------------------------------------------------

# Note: while this looks more verbose than it need be, the separations make for easier testing/checking in the "main"

# --------------------------------------------------
# Hard coded file name - for now at least

def Specific_JsonFileref():
	return "specific.json"

# --------------------------------------------------
# Save to JSON 

def Save_Specific_IntoJsonFile():
	global The_Specific
	defname = "Save_Specific_IntoJsonFile" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
	if len( The_Specific ) > 0 :
		str_fileref = Specific_JsonFileref()
		didok = SaveIntoJsonFile( The_Specific, str_fileref)
	else :
		print( defname + "Failed" )
	if debug :
		print( defname + "ENDED" )

# --------------------------------------------------
# Load from JSON 

def Convert_str_to_bool( p_s ):
	# in which we'll assume we've been passed a string
	i_s = p_s.lower()
	if i_s in ( "true", "yes", "y" ) : 
		return True
	elif i_s in ( "false", "no", "n" ) :
		return False
	else :
		return None 

def Convert_JSONitem_to_bool( p_v ):
	# really, the stock Pytho JSON load _should_ have detected the JSON booleans already, but here we'll cover strings too 
	if type( p_v ) is bool :
		return p_v
	elif type( p_v ) is str :
		return Convert_str_to_bool( p_v )
	else :
		return None

# use manual direct references to the enumerations

def Convert_Incoming_ToMatchTypeOf_Proforma( p_incoming, p_proforma ):
	# do conversion to enumeration
	defname = "Convert_Incoming_ToMatchTypeOf_Proforma" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
		print( defname + "p_proforma" )
		print( p_proforma )
		print( defname + "p_incoming" )
		print( p_incoming )
	if type( p_proforma ) is enum_LeMode :
		if debug :
			print( defname + "Value converting to enum_LeMode" )
		r_outcoming = enum_LeMode( p_incoming)
	elif type( p_proforma ) is enum_Version :
		if debug :
			print( defname + "Value converting to enum_Version" )
		r_outcoming = enum_Version( p_incoming)
	elif type( p_proforma ) == type( p_incoming ) :
		if debug :
			print( defname + "Value done as unchanged type" )
		r_outcoming = p_incoming
	elif type( p_proforma ) is int :
		if debug :
			print( defname + "Value converting to int" )
		r_outcoming = int( p_incoming)
	elif type( p_proforma ) is str :
		if debug :
			print( defname + "Value converting to str" )
		r_outcoming = str( p_incoming)
	elif type( p_proforma ) is bool :
		if debug :
			print( defname + "Value converting to bool" )
		r_outcoming = Convert_JSONitem_to_bool( p_incoming )
	else :
		if debug :
			print( defname + "Fallback: Value done as unchanged type" )
		r_outcoming = p_incoming
	if debug :
		print( "r_outcoming" )
		print( r_outcoming )
		print( defname + "ENDED" )
	return r_outcoming

# use a list of enumerations

def Convert_Incoming_ToMatchTypeOf_Proforma_ListEnumTypes( p_incoming, p_proforma, p_lst_enumeration_types ):
	# do conversion to enumeration
	defname = "Convert_Incoming_ToMatchTypeOf_Proforma_ListEnumTypes" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
		print( defname + "p_proforma" )
		print( p_proforma )
		print( defname + "p_incoming" )
		print( p_incoming )
	did_as_enum = False
	for an_enum_typ in p_lst_enumeration_types :
		if type( p_proforma ) is an_enum_typ :
			if debug :
				print( defname + "Value converting to an_enum_typ" )
			r_outcoming = an_enum_typ( p_incoming)
			did_as_enum = True
			break
	if did_as_enum :
		pass # as we've already done as we need
	elif type( p_proforma ) == type( p_incoming ) :
		if debug :
			print( defname + "Value done as unchanged type" )
		r_outcoming = p_incoming
	elif type( p_proforma ) is int :
		if debug :
			print( defname + "Value converting to int" )
		r_outcoming = int( p_incoming)
	elif type( p_proforma ) is str :
		if debug :
			print( defname + "Value converting to str" )
		r_outcoming = str( p_incoming)
	elif type( p_proforma ) is bool :
		if debug :
			print( defname + "Value converting to bool" )
		r_outcoming = Convert_JSONitem_to_bool( p_incoming )
	else :
		if debug :
			print( defname + "Fallback: Value done as unchanged type" )
		r_outcoming = p_incoming
	if debug :
		print( "r_outcoming" )
		print( r_outcoming )
		print( defname + "ENDED" )
	return r_outcoming

def list_enum_values_of_dict_key( p_dct_proforma ):
	defname = "list_enum_values_of_dict_key" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
		print( "p_dct_proforma" )
		print( p_dct_proforma )
	r_lst = []
	if len( p_dct_proforma ) > 0 :
		if debug :
			print( defname + "non-empty p_dct_proforma" )
		i_example_key = list( p_dct_proforma.keys() )[0]
		if debug :
			print( defname + "i_example_key" )
			print( i_example_key )
			print( type( i_example_key ) )
		if isinstance( i_example_key, Enum):
			if debug :
				print( defname + "type( i_example_key ) is Enum" )
			r_lst = list( map( lambda c: c.value, type( i_example_key ) ) )
			print( defname + "r_lst" )
			print( r_lst )
		else:
			if debug :
				print( defname + "Not an enumeration!" )
	else:
		if debug :
			print( defname + "Empty p_dct_proforma" )
	if debug :
		print( defname + "ENDED" )
	return r_lst

def list_enums_of_dict_key( p_dct_proforma ):
	defname = "list_enums_of_dict_key" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
		print( "p_dct_proforma" )
		print( p_dct_proforma )
	r_lst = []
	if len( p_dct_proforma ) > 0 :
		if debug :
			print( defname + "non-empty p_dct_proforma" )
		i_example_key = list( p_dct_proforma.keys() )[0]
		if debug :
			print( defname + "i_example_key" )
			print( i_example_key )
			print( type( i_example_key ) )
		if isinstance( i_example_key, Enum):
			if debug :
				print( defname + "type( i_example_key ) is Enum" )
			r_lst = list( type( i_example_key ) )
			print( defname + "r_lst" )
			print( r_lst )
		else:
			if debug :
				print( defname + "Not an enumeration!" )
	else:
		if debug :
			print( defname + "Empty p_dct_proforma" )
	if debug :
		print( defname + "ENDED" )
	return r_lst

def MatchKey_InKey_DctProforma( p_key_JSD, p_dct_PPF):
	# see if there's a key in the proforma that matches the in key
	# if the ProForma has an enumeration then check against all values in the enumeration
	# otherwise check the actual keys in the ProForma dictionary - indeed this is why it was passed through as a parameter
	defname = "MatchKey_InKey_DctProforma" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
		print( defname + "p_key_JSD" )
		print( p_key_JSD )
		print( defname + "p_dct_PPF" )
		print( p_dct_PPF )
	# preset outputs
	r_PPF_key_matched = False
	r_PPF_matching_key = None
	r_PPF_example_val = None
	# do we have an enumeration for a key in the ProForma ?
	# - get one of the keys
	if len( p_dct_PPF ) > 0 :
		lst_keys_proforma = list( p_dct_PPF.keys() ) # because keys() is not actually a list - see elsewhere
		# we need to protect ourselves against using index zero on an emptiness 
		if len( lst_keys_proforma ) > 0 :
			for i_key_PPF_consider in lst_keys_proforma : # just grab the first key
				if debug :
					print( defname + "i_key_PPF_consider" )
					print( i_key_PPF_consider )
				if isinstance( i_key_PPF_consider, Enum):
					# then we need to compare against all the values of the enumeration to find the matching particular enumeration
					if debug :
						print( defname + "i_key_PPF_consider IS an Enumeration!" )
					i_lst_key_enumerations = list( type( i_key_PPF_consider ) )
					if debug :
						print( defname + "i_lst_key_enumerations" )
						print( i_lst_key_enumerations )
					if debug :
						i_lst_key_values = list( map( lambda c: c.value, i_lst_key_enumerations ) )
						print( defname + "i_lst_key_values" )
						print( i_lst_key_values )
					for key_enum in i_lst_key_enumerations :
						if p_key_JSD == key_enum.value :
							r_PPF_key_matched = True
							r_PPF_matching_key = key_enum 
							r_PPF_example_val = p_dct_PPF[ i_key_PPF_consider ]
							break
			if not r_PPF_key_matched :
				if debug :
					print( defname + "i_key_PPF_consider is NOT an Enumeration!" )
				# now we need to compare against all the keys in this level of the ProForma, we can just loop even though the keys might be empty
				for key_val in p_dct_PPF.keys() :
					if p_key_JSD == key_val :
						r_PPF_key_matched = True
						r_PPF_matching_key = key_val # actually either that or key_val will do here
						r_PPF_example_val = p_dct_PPF[ r_PPF_matching_key ]
						break
		else:
			if debug :
				print( defname + "lst_keys_proforma is empty!" )
	else:
		if debug :
			print( defname + "p_dct_PPF is empty!" )
	if debug :
		print( defname + "r_PPF_key_matched" )
		print( r_PPF_key_matched )
		print( defname + "r_PPF_matching_key" )
		print( r_PPF_matching_key )
		print( defname + "r_PPF_example_val" )
		print( r_PPF_example_val )
		# input( defname + "Press ENTER to end " + defname )
		print( defname + "ENDED" )
	return r_PPF_key_matched, r_PPF_matching_key, r_PPF_example_val

# Take in a text only dicionary, and use a ProForma dictionary to guide the conversion of values into Enumerations
# We'll assume to work only on two supplied dictionaries
# therefore we start by comparing keys
# then work on the values - those values might be:
# - a simple value
# - a list
# - a dictionary - in which case we can recurse
#

# =========================================
# Re-write as neater structural procedures

def Make_PythonList_From_JsonData_AsPer_PythonProForma( p_lst_JSD, p_lst_PPF, p_lst_enumeration_types, p_depth ):
	defname = "Make_PythonList_From_JsonData_AsPer_PythonProForma" + ":> "
	# No need to re-check the parameter types as we'll only call it after already doing that - yay Python ?!?
	debug = False
	if debug :
		print( defname + "BEGIN" + " Depth: " + str(p_depth) )
		print( defname + "p_lst_JSD" )
		print( p_lst_JSD )
		print( defname + "p_lst_PPF" )
		print( p_lst_PPF )
		# input( "Press ENTER when ready")
	r_lst = []
	if len( p_lst_PPF ) :
		# this is only going to work if there is something in the ProForma list
		for obj_JSD in p_lst_JSD :
			if debug :
				print( defname + "At list item in JSONdata:" )
			t_lst_item = Make_PythonObject_From_JsonData_AsPer_PythonProForma( obj_JSD, p_lst_PPF[ 0 ], p_lst_enumeration_types, p_depth + 1 )
			if not t_lst_item is None :
				if debug :
					print( defname + "r_lst.append( t_lst_item )")
				r_lst.append( t_lst_item )
			else :
				if debug :
					print( defname + "Did not get a usable object for storing" )
				pass
	return r_lst

def Make_PythonDict_From_JsonData_AsPer_PythonProForma( p_dct_JSD, p_dct_PPF, p_lst_enumeration_types, p_depth ):
	defname = "Make_PythonDict_From_JsonData_AsPer_PythonProForma" + ":> "
	# No need to re-check the parameter types as we'll only call it after already doing that - yay Python ?!?
	debug = False
	if debug :
		print( defname + "BEGIN" + " Depth: " + str(p_depth) )
		print( defname + "p_dct_JSD" )
		print( p_dct_JSD )
		print( defname + "p_dct_PPF" )
		print( p_dct_PPF )
		# input( "Press ENTER when ready")
	r_dct = {}
	for k_JSD in p_dct_JSD.keys() :
		if debug :
			print( defname + "At key in JSONdata: " + k_JSD )
		# find if the ProForma has a matching Key
		i_PPF_key_matched, i_PPF_matching_key, i_PPF_example_val = MatchKey_InKey_DctProforma( k_JSD, p_dct_PPF )
		if i_PPF_key_matched :
			if debug :
				print( defname + "Is a matched key: ")
				print( i_PPF_matching_key )
			# Handle the Values
			if debug :
				print( defname + "p_dct_JSD[ k_JSD]")
				print( p_dct_JSD[ k_JSD] )
				print( defname + "i_PPF_example_val")
				print( i_PPF_example_val )
				# input( "Press ENTER when ready to recurse")
			t_dct_item = Make_PythonObject_From_JsonData_AsPer_PythonProForma( p_dct_JSD[ k_JSD], i_PPF_example_val, p_lst_enumeration_types, p_depth + 1 )
			if not t_dct_item is None :
				if debug :
					print( defname + "r_dct[ i_PPF_matching_key ] = t_dct_item")
				r_dct[ i_PPF_matching_key ] = t_dct_item
			else :
				if debug :
					print( defname + "Did not get a usable object for storing" )
				pass
		else :
			if debug :
				print( defname + "Ignoring unrecognised JSONdata key" )
			pass
	return r_dct

def MatchableStructuralType( p_obj_J, p_obj_P ):
	# Note: the parameter order probably doesn't matter, but we assume the convention anyway in case e later need it to
	if type( p_obj_J) is dict :
		if type( p_obj_P) is dict :
			return True
		else:
			return False
	elif type( p_obj_J) is list :
		if type( p_obj_P) is list :
			return True
		else:
			return False
	else:
		# actually this is a gross simplification as there are many "structural" types we can't yet handle, e.g. sets, named tuples etc 
		# but we can deal with those later as developement porgresses
		return False

def Make_PythonObject_From_JsonData_AsPer_PythonProForma( p_obj_JSD, p_obj_PPF, p_lst_enumeration_types, p_depth ):
	defname = "Make_PythonObject_From_JsonData_AsPer_PythonProForma" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" + " Depth: " + str(p_depth) )
		print( defname + "p_obj_JSD" )
		print( p_obj_JSD )
		print( defname + "p_obj_PPF" )
		print( p_obj_PPF )
		# input( "Press ENTER when ready")
	r_obj = None
	if MatchableStructuralType( p_obj_JSD, p_obj_PPF ) :
		if debug :
			print( defname + "MatchableStructuralType returned True" )
		# now we split by the type to call various sub-functions, for this we're guided by the type of the ProForma 
		if type( p_obj_PPF) is dict :
			if debug :
				print( defname + "Will call process for Dict" )
			r_obj = Make_PythonDict_From_JsonData_AsPer_PythonProForma( p_obj_JSD, p_obj_PPF, p_lst_enumeration_types, p_depth )
		elif type( p_obj_PPF) is list :
			if debug :
				print( defname + "Will call process for List" )
			r_obj = Make_PythonList_From_JsonData_AsPer_PythonProForma( p_obj_JSD, p_obj_PPF, p_lst_enumeration_types, p_depth )
		else :
			if debug :
				print( defname + "IS a structured type but we don't handle that!" )
	else :
		if debug :
			print( defname + "Not a structured type so just convert" )
		if False:
			r_obj = Convert_Incoming_ToMatchTypeOf_Proforma( p_obj_JSD, p_obj_PPF )
		else :
			r_obj = Convert_Incoming_ToMatchTypeOf_Proforma_ListEnumTypes( p_obj_JSD, p_obj_PPF, p_lst_enumeration_types )
	return r_obj


# =========================================

def Load_Specific_FromJsonFile( p_lst_enumeration_types ):
	global The_Specific
	defname = "Load_Specific_FromJsonFile" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
	str_fileref = Specific_JsonFileref()
	gotok, specific_loading = LoadFromJsonFile( str_fileref )
	if gotok :
		if debug :
			print( defname + "did LoadFromJsonFile ok" )
		The_Specific = Make_PythonObject_From_JsonData_AsPer_PythonProForma( specific_loading, The_Specific, p_lst_enumeration_types, 1 )
	else :
		print( defname + "LoadFromJsonFile Failed" )
	if debug :
		print( defname + "ENDED" )

# --------------------------------------------------
# Failsafe JSON file handling
# --------------------------------------------------

def Saving_Specifics_Into_JSON():
	defname = "Saving_Specifics_Into_JSON" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
		print( defname + "Saving The Specific" )
	Save_Specific_IntoJsonFile()
	if debug :
		print( defname + "ENDED" )

def Loading_Specifics_From_JSON( p_lst_enumeration_types ):
	defname = "Loading_Specifics_From_JSON" + ":> "
	debug = False
	if debug :
		print( defname + "BEGIN" )
		print( defname + "Loading The Specific" )
	Load_Specific_FromJsonFile( p_lst_enumeration_types )
	if debug :
		print( defname + "ENDED" )

# --------------------------------------------------
# module intialisation
# --------------------------------------------------
# while the first booolean is True then the internal setting overrides by being written out to file
# So to make production ready, just change the first True to False
# the second is just there to allow temporary testing independent of the currently saved JSON files 

# Change this next line to True when development is complete, so that use of the  trying something but wanting the JSON file unaffected
if False :
	Loading_Specifics_From_JSON( [ enum_LeMode, enum_Version ] )


# --------------------------------------------------
# development test
# --------------------------------------------------

def module_test_check():
	defname = "module_test_check" + ":> "
	print( defname + "BEGIN" )
	print( defname + "Literal_ProForma_Enum" )
	print( Literal_ProForma_Enum )
	print( defname + "Literal_Specific_Enum" )
	print( Literal_Specific_Enum )
	print( defname + "Literal_Specific_AllStrings" )
	print( Literal_Specific_AllStrings )
	input( defname + "Press ENTER to run Make_PythonObject_From_JsonData_AsPer_PythonProForma( Literal_Specific_AllStrings, Literal_ProForma_Enum, 1 )" )
	New_Specific = Make_PythonObject_From_JsonData_AsPer_PythonProForma( Literal_Specific_AllStrings, Literal_ProForma_Enum, [ enum_LeMode, enum_Version ], 1 )
	print( defname + "New_Specific" )
	print( New_Specific )
	input( defname + "Press ENTER to continue after " + defname )
	print( defname + "ENDED" )

# --------------------------------------------------
# Manual check run
# --------------------------------------------------

def module_manual_check():
	defname = "module_manual_check" + ":> "
	print( defname + "BEGIN" )
	#
	print( defname + "Resetting The_Specific = Literal_Specific_Enum" )
	The_Specific = Literal_Specific_Enum
	#
	print( "Before Save and Load" )
	print( "The_Specific" )
	print( The_Specific )
	# 
	Saving_Specifics_Into_JSON()
	Loading_Specifics_From_JSON( [ enum_LeMode, enum_Version ] )
	#
	print( "After Save and Load" )
	print( "The_Specific" )
	print( The_Specific )
	#
	print( defname + "ENDED" )


# --------------------------------------------------
# main to execute, only if run as a program
# --------------------------------------------------
if __name__ == "__main__":
	defname = "__main__" + ":> "
	debug = True
	if debug :
		print( defname + "BEGIN" )
	if True :
		module_test_check()
	# 
	# Chance to do explicit 
	if True :
		module_manual_check()
	#
	if debug :
		print( defname + "ENDED" )
